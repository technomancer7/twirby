#!/usr/local/bin/ruby
class TwPassage
  attr_accessor :title
  attr_accessor :tags
  attr_accessor :body
  attr_accessor :links
  
  def initialize tw, title
    @title = title
    @links = []
    @body = ""
    @tags = []
    @file = nil
    @tw = tw
  end
  def append txt
    @body = "#{@body}\n#{txt}" if @body != ""
    @body = txt if @body == ""
  end
  def file=(f)
    @file = f
  end
  def update
    @links = []

    sl = @body.scan /\[\[[A-Za-z0-9;:()|\s]*\]\]/
    # iterate, if the string contains \ then handle differently
    for link in sl
      text = link.gsub("[[", "").gsub("]]", "")
      if text.include? "|"
        twt = TwLink.new()
        twt.label = text.split("|")[0]
        twt.next = text.split("|")[1]
        @tw.links << twt.next
        @links << twt
      else
        twt = TwLink.new()
        twt.label = text
        twt.next = text
        @tw.links << twt.next
        @links << twt
      end
    end
    # With setters
    slws = @body.scan /\[\[[A-Za-z0-9;:()|\s]*\]\[[$'"A-Za-z0-9;:()|\s]*\]\]/
    # Handle first part same as normal links
    for link in slws
      #puts "Adding slink #{link}"
      text = link.gsub("[[", "").gsub("]]", "")
      parts = text.split("][")
      if parts[0].include? "|"
        twt = TwLink.new()
        twt.label = parts[0].split("|")[0]
        twt.next = parts[0].split("|")[1]
        twt.setter = parts[1]
        @tw.links << twt.next
        @links << twt
      else
        twt = TwLink.new()
        twt.label = parts[0]
        twt.next = parts[0]
        twt.setter = parts[1]
        @tw.links << twt.next
        @links << twt
      end
    end
  end
  def file; if @file == nil; return @tw.file; else; return @file; end; end
end

class TwLink
  attr_accessor :next
  attr_accessor :label
  attr_accessor :setter
  
  def initialize #passage
    #@passage = passage
    @next = ""
    @label = ""
    @setter = ""
  end

  def to_s
    "[[#{@label}->#{@next}]]"
  end
end

class Twirby
  attr_reader :file
  attr_reader :passages
  attr_accessor :links
  def initialize path, type=:file, extensions=['tw', 'twee']
    @file = nil
    @passages = []
    @story_scripts = []
    @story_styles = []
    @title = ""
    @data = nil
    @titles = []
    @links = []
    if type == :dir
      Dir.foreach(path) do |entry|
        if entry.end_with? ".js"
        end
        if entry.end_with? ".css"
        end
        for e in extensions
          if entry.end_with? ".#{e}"
            #puts "Opening file #{entry}"
            @in_file = entry
            parseTw File.read(entry)
          end
        end
      end
    end
  end

  def parseTw text
    #puts "Parsing file..."
    @in_passage = nil
    for ln in text.split("\n")
      if ln.start_with? ":: "
        @in_passage.update if @in_passage != nil
        @ititle = ln.split(" ")[1..-1].join(" ")
        @itags = []
        if @ititle.include? "[" and @ititle.include? "]" and not @ititle.include? "\\[" and not @ititle.include? "\\]"
          @itags = @ititle.split("[")[1].split("]")[0].split(" ")
          @ititle = @ititle.split("[")[0].strip()
        end
        @titles << @ititle.dup
        #puts @titles.join(":")
        @passages << @in_passage = TwPassage.new(self, @ititle)
        @in_passage.file = @in_file if @in_file != nil
        @in_passage.tags = @itags
      else
        @in_passage.append ln if @in_passage != nil
      end
    end
  end
  attr_reader :titles
end

if __FILE__ == $0
  if ARGV[0] == "validate"
    st = Twirby.new Dir.pwd, :dir
    for pa in st.passages
      unless st.links.include? pa.title
        puts "Passage #{pa.title} is never linked to."
      end
      for link in pa.links
        exists = false
        unless st.titles.include? link.next
          puts "Link #{link} in passage #{pa.title}, target is undefined"
        end
      end
    end
  elsif ARGV[0] == "find"
    st = Twirby.new Dir.pwd, :dir
    for pa in st.passages
      puts "-------------\n\nTITLE: #{pa.title} (#{pa.tags})\n#{pa.body}\n#{pa.links}" if pa.title.include? ARGV[1..-1].join(" ")
    end
  end
end
